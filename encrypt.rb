def encrypt(value, key)
	value_to_string_array = value.chars # string arguments to array of characters by .chars
	value_to_ascii_array = value.chars.map {|char| char.ord} # iterate and find the ASCII by .ord
	shifted = value_to_ascii_array.map {|char| char+key} # shifting the letters by the value provided to the key
	puts shifted.map { |char| char.chr }.join
end
encrypt("Dipendra", 7)

def decrypt(value, key)
	value_to_string_array = value.chars 
	value_to_ascii_array = value.chars.map {|char| char.ord} 
	shifted = value_to_ascii_array.map {|char| char-key} # deducting the value of key to decrypt
	puts shifted.map { |char| char.chr }.join
end
decrypt("Z~hz{prh", 7)